#! /usr/bin/python

from itertools import product;

width = 3;
height = 3;
mid = width / 2;

def printComponent(x, y, width, height):
    print("    component tile_"+str(w)+str(h) )
    print("        walkable: [0..1] init 1;")
    print("")
    if (mid == x):
        print("        true -> (walkable' = walkable);")
    else:
        print("        initialization.active = 1 -> choice:((walkable' = 0)) + choice:((walkable' = 1));")
        print("        initialization.active = 0 -> (walkable' = walkable);")

    print( "    endcomponent //tile"+str(x) + str(y));
    print("")

def tileStr(x,y):
    return "tile_"+str(x) + str(y)

def printRobotCommands(x, y, width, height):
#robot commands:
#   1. while initializing --> do nothing
#   2. if on wall --> failed, do nothing
#   3. if goal reached    --> do nothing
#   4. on valid tile:
#       from the current robot direction, test whether there is a walkable tile adjacent left, in front, right, behind
#       go into the direction of the first successful test
    mid = width / 2;

    print ("        initialization.active != 1 & x="+str(x)+" & y="+str(y)+" & tile_"+str(x)+str(y)+".walkable = 0 -> (x'=x) & (y'=y) & (dir'=dir); //walked into wall-->program failed" )
    tile_ok_str = "        initialization.active != 1 & x="+str(x)+" & y="+str(y)+" & tile_"+str(x)+str(y)+".walkable = 1 "
    if y+1 < height:
        print (tile_ok_str + "& tile_"+str(x)+str(y+1)+".walkable = 1 -> (x'=x) & (y' = y+1) & (dir'= dir); //greedy: if walking straight ahead is possible, do it")
        if (x > 0):    
            print (tile_ok_str + "& " + tileStr(x,y+1)+".walkable = 0 & dir = 0 & "+tileStr(x-1,y)+".walkable = 1 -> (x' = x+(-1)) & (y' = y) & (dir' = dir); //can and should go left --> go")
            print (tile_ok_str + "& " + tileStr(x,y+1)+".walkable = 0 & dir = 0 & "+tileStr(x-1,y)+".walkable = 0 -> (x' = x)   & (y' = y) & (dir' = 1);   //can't but should go left --> turn around")
        else:
            print (tile_ok_str + "& " + tileStr(x,y+1)+".walkable = 0 & dir = 0 -> (x' = x)   & (y' = y) & (dir' = 1);   //is at left edge and should go left --> turn around")
            
        if x+1 < width:         
            print (tile_ok_str + "& " + tileStr(x,y+1)+".walkable = 0 & dir = 1 & "+tileStr(x+1,y)+".walkable = 1 -> (x' = x+1) & (y' = y) & (dir' = dir); //can and should go right --> go")
            print (tile_ok_str + "& " + tileStr(x,y+1)+".walkable = 0 & dir = 1 & "+tileStr(x+1,y)+".walkable = 0 -> (x' = x)   & (y' = y) & (dir' = 0);   //can't but should go right --> turn around")
        else:
            print (tile_ok_str + "& " + tileStr(x,y+1)+".walkable = 0 & dir = 1 -> (x' = x)   & (y' = y) & (dir' = 0);   //is at right edge and should go right --> turn around")
    elif y+1 == height:
        if (x > 0):
            print (tile_ok_str + "& dir = 0 & "+tileStr(x-1,y)+".walkable = 1 -> (x' = x+(-1)) & (y' = y) & (dir' = dir); //can and should go left --> go")
            print (tile_ok_str + "& dir = 0 & "+tileStr(x-1,y)+".walkable = 0 -> (x' = x)   & (y' = y) & (dir' = 1);   //can't but should go left --> turn around")
        else:
            print (tile_ok_str + "& dir = 0 -> (x' = x) & (y' = y) & (dir' = 1); //is in top left corner and should go left --> turn around")
        if (x+1 < width):
            print (tile_ok_str + "& dir = 1 & "+tileStr(x+1,y)+".walkable = 1 -> (x' = x+1) & (y' = y) & (dir' = dir); //can and should go right --> go")
            print (tile_ok_str + "& dir = 1 & "+tileStr(x+1,y)+".walkable = 0 -> (x' = x)   & (y' = y) & (dir' = 0);   //can't but should go right --> turn around")
        else:
            print (tile_ok_str + "& dir = 1 -> (x' = x) & (y' = y) & (dir' = 0); //is in top right corner and should go right --> turn around")
            
        
            
   
    print("");
    
    

print ("component main")
print ("")
print ("    component initialization")
print ("        active: [0..1] init 1;")
print ("        true -> (active' = 0);")
print ("    endcomponent")
print ("")

for w in range(width):
    for h in range(height):
        printComponent(w, h, width, height)
        
print ("    component robot")
print ("        x:     [0.." + str(width-1)+"]  init "+str(mid) + ";")
print ("        y:     [0.." + str(height-1)+"] init 0;")
print ("        dir:   [0..1] init 1; // 0 = left, 1 = right")
#print ("        state: [0..2] init 0; // 0=waiting for initialization; 1=active; 2 = finished; 3 = failed")
print ("")
print ("        initialization.active = 1 -> (x'=x) & (y'=y) & (dir' = dir);")
#print ("        initialization.active == 0 & (state == 2 | state == 3) -> (x'=x & y'=y & dir'=dir & state' = state);")
for w in range(width):
    for h in range(height):
        printRobotCommands(w, h, width, height)
print ("    endcomponent //robot")


print ("endcomponent //main")

